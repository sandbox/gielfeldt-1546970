<?php
/**
 * @file
 *
 * Pages for taxonomy mpath settings.
 */

/**
 * Form build for the settings form
 *
 * @ingroup forms
 */
function core_override_settings_form() {
  $form = array();

  $form['core_override_enabled'] = array(
    '#title' => t('Enable core overrides'),
    '#description' => t('Enable core overrides'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('core_override_enabled', FALSE),
  );
  $form['core_override_path'] = array(
    '#title' => t('Path to core override modules'),
    '#description' => t('This path must exist with DOCUMENT ROOT. If blank %fallback is used', array('%fallback' => variable_get('file_public_path', conf_path() . '/files') . '/core-override')),
    '#type' => 'textfield',
    '#default_value' => variable_get('core_override_path', ''),
  );

  $form['rebuild_overrides'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild core overrides'),
    '#submit' => array('core_override_rebuild_overrides_submit'),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'core_override_settings_form_submit';
  return $form;
}

function core_override_settings_form_submit($form, &$form_state) {
  $override = $form_state['values']['core_override_enabled'];
  if ($override) {
    _core_override_create_overrides();
  }
}

function core_override_rebuild_overrides_submit($form, &$form_state) {
  _core_override_create_overrides();
}

function core_override_overview_form() {
  $form = array();
  $overrides = core_override_get_overrides();
  $functions = core_override_get_mapped_functions();

  $header = array('Core file', 'Core function', 'Override module', 'Override function', 'Status');
  $rows = array();
  $values = array();

  $form['overview'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#multiple' => TRUE,
  );

  // Build table select
  foreach ($overrides as $function => $info) {
    $status = NULL;
    $status_msg = t('OK. Function is @status', array('@status' => $info['enabled'] ? t('enabled') : t('disabled')));
    if (!$info['compatible']) {
      $form['overview'][$function]['#disabled'] = TRUE;
      $status = WATCHDOG_ERROR;
      $status_msg = t('NOT OK. Function is not compatible with Drupal @version, only Drupal @versions', array(
        '@version' => VERSION,
        '@versions' => implode(', ', $info['versions'])
      ));
    }
    $status_img = '<img src="' . url('misc/') . ($status == NULL ? 'message-16-ok.png' : 'message-16-error.png') . '"/>';

    $rows[$function] = array(
      $functions[$function],
      $function,
      $info['module'],
      $info['callback'],
      array('data' => $status_img, 'title' => $status_msg)
    );
    $values[$function] = $info['enabled'];
  }
  $form['overview']['#options'] = $rows;
  $form['overview']['#default_value'] = $values;

  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

function core_override_overview_form_submit($form, &$form_state) {
  foreach ($form_state['values']['overview'] as $key => $value) {
    variable_set('core_override_enabled:' . $key, ($value) ? 1 : 0);
  }
  _core_override_create_overrides();
  drupal_set_message(t('The configuration options have been saved.'));
}

